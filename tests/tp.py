# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if(n>0):
        return "S" +nombre_entier(n-1)
    else:
        return "0"

def S(n: str) -> str:
    return "S"+n


def addition(a: str, b: str) -> str:
    return nombre_entier( len(a) + len(b) - 2)


def multiplication(a: str, b: str) -> str:
    return nombre_entier((len(a)-1) * (len(b)-1))


def facto_ite(n: int) -> int:
    facto_ite = 1
    for i in range(1, n+1):
        facto_ite *= i
    return facto_ite


def facto_rec(n: int) -> int:
    if(n>0):
        return n*facto_rec(n-1)
    else:
        return 1


def fibo_rec(n: int) -> int:
    if(n==0):
        return 0
    if(n==1):
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    if(n==0):
        return 0
    if(n==1):
        return 1
    else:
        a=1
        b=1
        c=1
        cpt = 2
        while cpt <n:
            c=a+b
            a=b
            b=c
            cpt+=1
        return c        


def golden_phi(n: int) -> int:
    if n == 0:
        return 1
    else:
        return 1 + 1 / golden_phi(n-1)



def sqrt5(n: int) -> int:
    return ((golden_phi(n)*2)-1)


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n % 2 == 0:
        return pow(a**2, n//2)
    else:
        return a * pow(a, n-1)

